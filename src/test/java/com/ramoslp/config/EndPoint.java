package com.ramoslp.config;

public interface EndPoint {
	
	// VideoGameDB
	public String VIDEOGAMES = "/videogames";
	public String SINGLE_VIDEOGAME = "/videogames/{videoGameId}";
	
	// football-data.org
	public String COMPETITIONS = "/competitions";

}