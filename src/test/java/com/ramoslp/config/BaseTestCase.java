package com.ramoslp.config;

import org.junit.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class BaseTestCase {
	
	public static RequestSpecification videogame_requestSpec;
	public static RequestSpecification football_requestSpec;
	public static ResponseSpecification responseSpec;
	
	@BeforeClass
	public static void setup() {
//		RestAssured.proxy("proxy.conductor.tecnologia", 9090);
		
		videogame_requestSpec = new RequestSpecBuilder().
				setBaseUri("http://localhost").
				setPort(8080).
				setBasePath("/app/").
				addHeader("Content-Type", "application/json").
				addHeader("Accept", "application/json").
				build();
		
		football_requestSpec = new RequestSpecBuilder().
				setBaseUri("http://api.football-data.org").
				setBasePath("/v2/").
				addHeader("X-Auth-Token", "ededcbda3ead463faafe678d14fff18d").
				addHeader("X-Response-Control", "minified").
				build();
		
		RestAssured.requestSpecification = videogame_requestSpec;
		
		responseSpec = new ResponseSpecBuilder().
				expectStatusCode(200).
				build();
		
		RestAssured.responseSpecification = responseSpec;
	}

}