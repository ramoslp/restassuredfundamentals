package com.ramoslp.test;

import static io.restassured.RestAssured.*;

import org.junit.Test;

import com.ramoslp.config.EndPoint;
import com.ramoslp.config.BaseTestCase;

public class MyFirstTest extends BaseTestCase {
	
	@Test
	public void myFirstTest() {
		given().
				log().
				all().
		when().get("videogames/1").
		then().
				log().
				all();
	}
	
	@Test
	public void getAllGames() {
		given().
				spec(videogame_requestSpec).
		when().get(EndPoint.VIDEOGAMES);
	}

}