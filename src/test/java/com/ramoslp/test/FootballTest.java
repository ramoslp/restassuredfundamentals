package com.ramoslp.test;

import static io.restassured.RestAssured.given;

import org.junit.Test;

import com.ramoslp.config.BaseTestCase;
import com.ramoslp.config.EndPoint;

public class FootballTest extends BaseTestCase {
	
	@Test
	public void getAllCompetitionsOneSeason() {
		given().
			spec(football_requestSpec).
			queryParam("season", 2018).
		when().
			get(EndPoint.COMPETITIONS).
		then().
			log().
			all();
			
	}

}