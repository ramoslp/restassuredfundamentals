package com.ramoslp.test;

import static io.restassured.RestAssured.*;

import org.junit.Test;

import com.ramoslp.config.BaseTestCase;
import com.ramoslp.config.EndPoint;

public class VideoGameDbTest extends BaseTestCase {
	
	@Test
	public void getAllGames() {
		given().
		when().
			get(EndPoint.VIDEOGAMES).
		then().
			log().
			all();
	}
	
	@Test
	public void createNewGameByJson() {
		String gameBodyJson = "{\n" + 
				"  \"id\": 11,\n" + 
				"  \"name\": \"My New Game\",\n" + 
				"  \"releaseDate\": \"2018-07-22T15:23:12.048Z\",\n" + 
				"  \"reviewScore\": 80,\n" + 
				"  \"category\": \"Coding\",\n" + 
				"  \"rating\": \"Mature\"\n" + 
				"}";
		
		given().
			body(gameBodyJson).
		when().
			post(EndPoint.VIDEOGAMES).
		then().
			log().
			all();
	}
	
	@Test
	public void createNewGameByXml() {
		String gameBodyXml = "<videoGame category=\"Coding\" rating=\"Universal\">\n" + 
				"    <id>12</id>\n" + 
				"    <name>My New Game 2</name>\n" + 
				"    <releaseDate>2005-10-01T00:00:00-03:00</releaseDate>\n" + 
				"    <reviewScore>85</reviewScore>\n" + 
				"  </videoGame>";
		
		given().
			body(gameBodyXml).
		when().
			post(EndPoint.VIDEOGAMES).
		then().
			log().
			all();
	}
	
	@Test
	public void updateGame() {
		String gameBodyJson = "{\n" + 
				"  \"id\": 12,\n" + 
				"  \"name\": \"My Updated Game\",\n" + 
				"  \"releaseDate\": \"2018-07-22T15:23:12.048Z\",\n" + 
				"  \"reviewScore\": 95,\n" + 
				"  \"category\": \"Coding\",\n" + 
				"  \"rating\": \"Mature\"\n" + 
				"}";
		
		given().
			body(gameBodyJson).
			pathParam("videoGameId", 12).
		when().
			put(EndPoint.SINGLE_VIDEOGAME).
		then().
			log().
			all();
	}
	
	@Test
	public void deleteGame() {
		given().
			pathParam("videoGameId", 11).
		when().
			delete(EndPoint.SINGLE_VIDEOGAME).
		then().
			log().
			all();
	}
	
	@Test
	public void getGameById() {
		given().
			pathParam("videoGameId", 7).
		when().
			get(EndPoint.SINGLE_VIDEOGAME).
		then().
			log().
			all();
	}

}